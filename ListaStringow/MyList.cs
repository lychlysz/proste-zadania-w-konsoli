﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaStringow
{
    class MyList : IEnumerator
    {
        int position = -1;
        string[] lista = new string[] {"Joł ziomeczku ASd", "Jak się masz asd", "ASD Co słychać ziomeczku",
                                        "ziomeczku Asd", "Jak tam rodzina asD","Elegancko ziomeczku"};

        public object Current => throw new NotImplementedException();

        public void PokazTablice()
        {
            foreach (var item in lista)
            {
                Console.Write(item);
                if (MoveNext())
                    Console.WriteLine(",");
            }
        }

        public string Liczba()
        {
            string temp = "";
            string[] separator = new string[] { " " };
            string[] tabWynik;
            int licznik = 0;
            string slowo = "";
            int licznikSlowa = 0;

            foreach (var item in lista)
            {
                temp = String.Concat(temp, item);
                temp += " ";
            }
            tabWynik = temp.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in tabWynik)
            {
                licznikSlowa = 0;
                foreach (var asd in tabWynik)
                {
                    if (item.Equals(asd, StringComparison.OrdinalIgnoreCase))
                        licznikSlowa++;
                }
                if (licznikSlowa > licznik)
                {
                    licznik = licznikSlowa;
                    slowo = item;
                }

            }

            return $"Najwięcej razy pojawiło sie słowo {slowo.ToLower()} - {licznik} razy.";
        }

        public bool MoveNext()
        {
            position++;
            return (position < lista.Length);
        }

        public void Reset()
        {
            position = -1;
        }
    }
}
