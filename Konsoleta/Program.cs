﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konsoleta
{
    class Program
    {
        static void Main(string[] args)
        {
            MyWord word = new MyWord();

            word.ReadWord();
            word.ChangeWord();
            word.Check();

            Console.ReadLine();
        }
    }
}
