﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konsoleta
{
    class MyWord
    {
        public string Word { get; set; }
        public string WordReverse { get; set; }

        public void ReadWord()
        {
            Word = Console.ReadLine();
        }

        public void ChangeWord()
        {
            string asd = "";

            for (int i = 0; i < Word.Length; i++)
            {
                asd += Word[(Word.Length - 1) - i];
            }
            WordReverse = asd;
        }

        public void Check()
        {
            if(Word.Equals(WordReverse, StringComparison.OrdinalIgnoreCase))
                Console.WriteLine($"słowo {Word} jest palidromem, gdyż po odwróceniu układa się w wyraz {WordReverse}");
            else
                Console.WriteLine($"słowo {Word} nie jest palidromem, gdyż po odwróceniu układa się w wyraz {WordReverse}");
        }
    }
}
